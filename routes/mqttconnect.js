var express = require('express');
var router = express.Router();
var mqtt = require('mqtt');
var client=null;

router.get('/', function (req, res) {
    res.render('mqttconnect');
});




router.post('/connect', function (req, res) {
    console.log("LLege");
    host = `mqtt://${req.body.server}`;
    options = {
        port: Number(req.body.port),
        username: req.body.user,
        password: req.body.password
    };
    console.log(host,":", options);

    client = mqtt.connect(host, options);

    client.on('connect', function () {
        console.log("---connect---");
        res.json({ data: "connect" });
    });

    client.on('error', function (err) {
        res.json({ data: "error" });
    });

});

module.exports =router;

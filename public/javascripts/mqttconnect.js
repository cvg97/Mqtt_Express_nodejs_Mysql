$(document).ready(function () {
    $('#connect').on('click', function(){
        connect();
    });
});



function connect() {
    var server = $('#server').val();
    var user = $('#user').val();
    var password = $('#password').val();
    var port = $('#port').val();



    if (server && user && password && port) {
       var object = {
            server:server,
            user:user,
            password:password,
            port:port
        };

        sendData(object);

    } else {
        alert('algun campo no se ha llenado');
    }
}


function sendData(data) {
    $.ajax({
        url: '/mqttconnect/connect',
        dataType:'json',
        data: data,
        type: 'POST', 
        success: function(response){
            alert('ok');
        }
    })
}
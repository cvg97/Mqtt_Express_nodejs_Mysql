var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mqtt = require('mqtt');
var mysql = require('mysql');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var mqttRouter = require('./routes/mqtt');
var mqttConnectRouter = require('./routes/mqttconnect');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/mqtt', mqttRouter);
app.use('/mqttconnect', mqttConnectRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

configmysql = {
    host:'localhost',
    user:'root'
};


var con = mysql.createConnection(configmysql);
con.connect();
con.query('show databases' , function(err, res){
    console.log(res, '\n', 'connectado a mysql');
});



options = {
    port:12629,
    username:"ppqxlmxf",
    password:"B1sPB6n-F7HP"  
};

host = "mqtt://m13.cloudmqtt.com";

var client = mqtt.connect(host, options );
client.on('connect', function(){
    
    console.log("--connect from save in database");
    client.subscribe('ingmanuel');
})

client.on('message', function(topic, message){
    console.log("canal 2", message);
    try {
        message = JSON.parse(message);
    
        con.query(`insert into data.arduino (device , temp , hum ) 
        values ("${message.device}", "${message.temp}", ${message.hum} ) `, function(err, res){
            if(err) throw err;
            console.log('--insert--', res);
        });

    } catch (error) {
        console.log(`el mensaje no era un JSON ${error}`);
    }
    

  
})

client.on('error', function(err){
    console.log(err);
})

module.exports = app;
